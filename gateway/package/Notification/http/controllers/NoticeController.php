<?php


namespace Package\Notification\http\controllers;

use Illuminate\Support\Carbon;
use Package\Notification\classes\sms;
use Package\Notification\model\Notice;

class NoticeController extends Controller
{

    public function send()
    {
        $this->validate(request(), [
            'mobile' => 'required|nullable',
            'type' => 'required',
            'provider' => 'required'
        ]);

        $mobile = request("mobile");
        $type = request("type");
        $provider = request("provider");

        $code = mt_rand(1000, 9999);
        $params = [
            'request' => ['mobile' => $mobile, 'message' => 'code:' . $code, 'code' => $code],
            'user_id' => null,
            'provider' => $provider,
            'status' => 'delivered',
            'type' => $type,
        ];


        $sms=new sms($params);
        $result=$sms->send();

        Notice::create([
            'type' => $type ,
            'provider' => $provider ,
            'status' => 'delivered',
            'user_id' => null,
            'request' => $params['request'],
            'response' => ['tracking_code'=> $result]
        ]);


        return response()->json([
            'data' => [
                'message' => __('notice::messages.send'),
                'status'  => 200
            ]
        ]);

    }

    public function checkCode()
    {
        $this->validate(request(), [
            'mobile' => 'required|nullable',
            'code' => 'required|int'
        ]);

        $mobile = request('mobile');
        $code = request('code');

        $notice = Notice::where([
            ['type', '=', 'verify_code'],
            ['request', 'LIKE', '%' . $code . '%'],
            ['request', 'LIKE', '%' . $mobile . '%'],
        ])->orderByDesc('notice_id')->first();
        if (!$notice)
            abort('404', __('notice::messages.code.invalid'));

        /*
        |--------------------------------------------------------------------------
        | Check Expire Code
        |--------------------------------------------------------------------------
        */
        $time=Carbon::now()->subMinutes(3);;
        if($time->gt($notice->created_at))
            abort('500',__('notice::messages.code.expired'));


        return response()->json([
            'data' => [
                'message' => __('notice::messages.code.success'),
                'status' => 200
            ]
        ]);


    }

}
