<?php


namespace Package\Notification\classes;


use GuzzleHttp\Client;
use Package\Notification\model\Notice;

class sms extends Notice
{

    public $error;
    public $params;
    protected $url = 'http://sms1000.ir/url';
    protected $fillable=['mobile','message'];


    public function __construct(array $attributes = [])
    {
        $this->params=$attributes;
        parent::__construct($attributes);
    }


    public function send()
    {

        try {
            $client = new Client();
            $result = $client->request('GET', $this->url.'/send.aspx', [
                'query' => [
                    'username' => config('sms.username'),
                    'password' => config('sms.password'),
                    'from' => config('sms.number'),
                    'to' => $this->params['request']['mobile'],
                    'farsi' => true,
                    'message' => $this->params['request']['message']
                ]
            ])->getBody()->getContents();
            $value=strstr($result,'<');
            $xml = simplexml_load_string($value, "SimpleXMLElement");
            $json = json_decode(json_encode($xml),TRUE);

        } catch (\Exception $exception) {

            $this->error = $exception->getMessage();
            return false;
        }
        return $json[0];
    }

    public function getInfo()
    {
        try {

            $client = new Client();
            $res2 = $client->request('GET', $this->url.'/GetInfoXML.aspx', [
                'query' => [
                    'username' => 'payeshgar',
                    'password' => 'p@yeshg@r',
                ]
            ])->getBody()->getContents();


        } catch (\Exception $exception) {
            dd($exception);
            $this->error = $exception->getMessage();
            return false;
        }
        return $array;
    }


    public function receive()
    {
        try {

            $client = new Client();
            $res2 = $client->request('GET', $this->url.'/receive.aspx', [
                'query' => [
                    'username' => 'payeshgar',
                    'password' => 'p@yeshg@r',
                ]
            ])->getBody()->getContents();


        } catch (\Exception $exception) {
            dd($exception);
            $this->error = $exception->getMessage();
            return false;
        }
        return $array;
    }
}
