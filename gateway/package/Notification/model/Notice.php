<?php


namespace Package\Notification\model;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{

    protected $table='notice';
    protected $primaryKey='notice_id';
    protected $fillable = ['user_id','type','request','response','status','provider'];

    protected $casts=[
        'request' => 'array',
        'response' => 'array'
    ];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

}
