<?php
return [
    'url' => 'http://sms1000.ir/webservice/smspro.asmx?WSDL',
    'username' => 'payeshgar',
    'password' => 'p@yeshg@r',
    'number'   => '500012012012012',
    'pattern' => [
        'operator' => [
            'send' => 'سفارش %number% به اپراتور سازمان ارسال شد',
            'verify' => 'سفارش %number% توسط اپراتور فروشگاه تایید شد',
            'cancelled'=>'متاسفانه سفارش %number% توسط اپراتور فروشگاه تایید نشد'
        ],
        'packaging'=>[
            'verify' => 'سفارش %number%  توسط پیک به مقصد ارسال شد',
            'cancelled' => 'سفارش %number%  توسط پیک به مقصد ارسال نشد'
        ],
        'delivery' => [
            'verify' =>'سفارش %number% به مقصد تحویل داده شد',
            'cancelled' =>'سفارش %number%  توسط پیک به مقصد تحویل داده نشد',
        ]
    ]
];
