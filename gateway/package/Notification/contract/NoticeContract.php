<?php


namespace Package\Notification\contract;


interface NoticeContract
{


    public function send();

    public function status();

}
