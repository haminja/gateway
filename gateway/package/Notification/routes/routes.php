<?php
/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Route Notification Package
|--------------------------------------------------------------------------
|
*/
$router=$this->app->router;


$router->group(['prefix'=>'/notice','namespace'=>'Package\Notification\http\controllers'], function () use ($router) {

    $router->post('/send','NoticeController@send');
    $router->get('/','NoticeController@checkCode');

});
