<?php

namespace Package\Notification;

use Illuminate\Support\ServiceProvider;
use Package\Notification\classes\sms;
use Package\Notification\contract\NoticeContract;
use Package\Notification\jobs\NoticeJob;

class NotificationServiceProvider extends ServiceProvider
{


    public function boot(){

        $this->loadRoutesFrom(__DIR__.'/routes/routes.php');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'notice');

        $this->mergeConfigFrom(
            __DIR__.'/config/sms.php', 'sms'
        );

    }

    public function register()
    {
        $this->app->bind(NoticeContract::class, sms::class);
    }
}
