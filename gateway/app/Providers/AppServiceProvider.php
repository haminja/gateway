<?php

namespace App\Providers;

use App\Classes\Auth\Authenticate;
use App\Classes\Request\Request;
use App\Classes\Service\DNSRegistry;
use App\Classes\Service\ServiceRegistryContract;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }


    public function boot(){

        $this->app->singleton(Request::class, function () {
            return $this->prepareRequest(Request::capture());
        });


        $this->app->bind(ServiceRegistryContract::class, DNSRegistry::class);

        $this->app->alias(Request::class, 'request');

        $this->app->singleton(Client::class,function (){
            return new Client([
                'timeout' => config('gateway.global.timeout'),
            ]);
        });

    }


    /**
     * Prepare the given request instance for use with the application
     * @param \App\Http\Request $request
     * @return  Request
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function prepareRequest(Request $request)
    {
        $request->setUserResolver(function () {
            return $this->app->make('auth')->user();
        })->setRouteResolver(function () {
            return $this->app->currentRoute;
        })->setTrustedProxies([
            '10.7.0.0/16', // Docker Cloud
            '103.21.244.0/22', // Cloud Flare
            '103.22.200.0/22',
            '103.31.4.0/22',
            '104.16.0.0/12',
            '108.162.192.0/18',
            '131.0.72.0/22',
            '141.101.64.0/18',
            '162.158.0.0/15',
            '172.64.0.0/13',
            '173.245.48.0/20',
            '188.114.96.0/20',
            '190.93.240.0/20',
            '197.234.240.0/22',
            '198.41.128.0/17',
            '199.27.128.0/21',
            '172.31.0.0/16', // Rancher
            '10.42.0.0/16' // Rancher
        ], \Illuminate\Http\Request::HEADER_X_FORWARDED_ALL);


        return $request;
    }



}
