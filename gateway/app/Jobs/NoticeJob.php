<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Log;
use Package\Notification\classes\sms;
use Package\Notification\model\Notice;

class NoticeJob extends Job
{

    public array $params;

    /**
     * Create a new job instance.
     *
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params=$params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $key=$this->params['key'];
        $pattern=config('sms.pattern.'.$key);

        $replaces=$this->params['replaces'];
        $result= strtr($pattern, $replaces);


        $params = [
            'request' => ['mobile' => $this->params['mobile'], 'message' => $result],
            'user_id' => $this->params['user_id'],
            'provider' => 'sms',
            'status' => 'delivered',
            'type' => 'single_send',
        ];

        $sms=new sms($params);
        $result=$sms->send();

        Notice::create([
            'type' => 'single_send' ,
            'provider' => 'sms' ,
            'status' => 'delivered',
            'user_id' => $this->params['user_id'],
            'request' => $params['request'],
            'response' => ['tracking_code'=> $result]
        ]);


    }
}
