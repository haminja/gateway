<?php


namespace App\Classes\Service;


class DNSRegistry implements ServiceRegistryContract
{

    public function resolveInstance($serviceId){
        $config = config('gateway');

        // If service doesn't have a specific URL, simply append global domain to service name

        $hostname = $config['services'][$serviceId]['hostname'] ?? $serviceId . '.' . $config['global']['domain'];

        return 'http://' .  $hostname;
    }

}
