<?php

namespace App\Classes\Service;

use App\Classes\Presenters\JSONPresenter;
use GuzzleHttp\Psr7\Response;

/**
 * Class RestResponse
 * @package App\Services
 */
class RestBatchResponse
{
    /**
     * @var array
     */
    protected $responses = [];

    /**
     * @var array
     */
    protected $codes = [];

    /**
     * @var int
     */
    protected $failures = 0;

    /**
     * @var bool
     */
    protected $hasCritical = false;

    /**
     * @param string $alias
     * @param Response $response
     */
    public function addSuccessfulAction($alias, Response $response)
    {
        $this->addAction($alias, (string)$response->getBody(), $response->getStatusCode());
    }

    /**
     * @param string $alias
     * @param Response $response
     */
    public function addFailedAction($alias, Response $response)
    {
        $this->addAction($alias, (string)$response->getBody(), $response->getStatusCode());
        $this->failures++;
    }

    /**
     * @param string $alias
     * @param $content
     * @param $code
     */
    private function addAction($alias, $content, $code)
    {
        $this->responses[$alias] = $content;
        $this->codes[$alias] = $code;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getResponses()
    {
        return collect($this->responses)->map(function ($response) {
            return JSONPresenter::safeDecode($response);
        });
    }

    /**
     * @param $batch
     * @return array
     */
    public function exportParameters($batch)
    {
        return collect(array_keys($this->responses))->reduce(function ($carry, $alias) use ($batch) {
            $output = [];
            $decoded = json_decode($this->responses[$alias], true);
            if ($decoded === null) return $carry;
            if ($explore = $batch->getExport()) {
                foreach ($explore as $key => $value) {

                    if (isset($decoded['data'][0])) {
                        $result = collect($decoded['data'])->transform(function ($val, $key) use ($value) {
                            $response = [];
                            if (is_array($value)) {
                                foreach ($value as $key => $item) {
                                    $response[$key] = isset($val[$item]) ? $val[$item] : $item;
                                }
                            } else {
                                $response[$value] = isset($val[$value]) ? $val[$value] : null;
                            }

                            return $response;
                        })->toArray();
                    }else{
                        $result= isset($decoded['data'][$value]) ? $decoded['data'][$value] : [] ;
                    }

                    $value = is_numeric($key) ? $value : $key ;

                    $output[$alias . '%' . $value] = $result;
                }
            }
            return array_merge($carry, $output);
        }, []);
    }

    /**
     * @return bool
     */
    public function hasFailedRequests()
    {
        return $this->failures > 0;
    }

    /**
     * @param bool $critical
     * @return $this
     */
    public function setCritical($critical)
    {
        $this->hasCritical = $critical;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasCriticalActions()
    {
        return $this->hasCritical;
    }

}
