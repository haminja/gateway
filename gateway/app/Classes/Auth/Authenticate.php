<?php


namespace App\Classes\Auth;


use Illuminate\Http\Request;

class Authenticate
{
    protected $url='http://auth.profatest.ir/';
    protected $request;
    protected $data;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->checkToken();
    }

    public function checkToken(){
        try {

            $client = new \GuzzleHttp\Client();
            $params=[
                'headers' => [
                    'Authorization' => $this->getToken(),
                    'Organization'  => $this->request->header('Organization')
                ]
            ];

            $res=$client->request('GET',$this->url.'checkToken',$params)->getBody()->getContents();
            $result=json_decode($res);
            if(!isset($result->data))
                abort($result->error->status,$result->error->message);

            $this->data = $result->data;
        }catch (\Exception $exception){
            abort($exception->getCode(),$exception->getMessage());
        }

        return true;
    }


    public function getUserId(){
        return $this->data->user->user_id;
    }

    public function getOrgId(){
        return optional($this->data->organization)->organization_id ?? '';
    }

    public function getRole(){
        return !empty($this->data->role) ? $this->user->role : '';
    }

    public function getToken(){
        return $this->request->header('Authorization');
    }

    public function getCity(){
        return $this->data->user->city_id;
    }

    public function getPlan(){
        return optional($this->data->organization)->plan_id;
    }
}
