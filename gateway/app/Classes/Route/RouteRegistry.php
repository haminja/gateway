<?php


namespace App\Classes\Route;


use Illuminate\Support\Str;
use Laravel\Lumen\Application;

class RouteRegistry
{

    protected $routes = [];


    public function __construct()
    {
        $this->parseConfigRoute();
    }


    public function getRoute($id){
        return collect($this->routes)->first(function($route) use ($id) {
            return $route->getId() == $id ;
        });

    }


    public function parseConfigRoute()
    {
        $config = config('gateway');
        collect($config['routes'])->each(function ($details) {

            if (! isset($details['id'])) {
                $details['id'] = (string)Str::uuid();
            }

            $route = new Route($details);

            collect($details['actions'])->each(function ($action, $key) use ($route) {
                $params = array_merge($action, ['alias' => $key]);
                $route->addAction(new Action($params));
            });

            $this->routes[] = $route;
        });

        return $this;

    }

    public function initRoute(Application $app)
    {
        collect($this->routes)->each(function ($route) use ($app){
            $method = $route->getMethod();

            $middleware=['checkRoute:'.$route->getId()];

            if (! $route->isPublic()) $middleware[] = 'auth';

            $path = $route->getPath();
            $app->router->{$method}($path, [
                'uses' => 'App\Http\Controllers\GateWayController@' . $method,
                'middleware' =>  $middleware
            ]);
        });
    }
}
