<?php


namespace App\Classes\Route;


class Route
{

    protected $config;
    protected $actions;

    public function __construct($options)
    {
        $this->config= $options;
    }


    public function addAction($action){
        $this->actions[] = $action;
    }


    public function getMethod(){
        return $this->config['method'];
    }

    public function getPath(){
        return $this->config['path'];
    }

    public function getId(){
        return $this->config['id'];
    }

    public function getConfig(){
        return $this->config;
    }

    public function getActions(){
        return collect($this->actions);
    }

    public function getSequence(){
        return collect($this->actions);
    }

    public function isAggregate(){
        return count($this->actions) > 1;
    }

    /**
     * @inheritdoc
     */
    public function isPublic()
    {
        return $this->config['public'] ?? false;
    }

}
