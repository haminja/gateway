<?php

namespace App\Http\Controllers;

use App\Classes\Request\Request;
use App\Classes\Service\RestClient;
use App\Exceptions\NotImplementedException;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class GateWayController extends Controller
{
    protected $config;

    protected $actions;
    /**
     * @var Client
     */
    protected $client;

    protected Request $request;

    /**
     * Create a new controller instance.
     *
     * @param Request $request
     * @param RestClient $client
     */
    public function __construct(Request $request, RestClient $client)
    {
        if (empty($request->getRoute())) abort('404', 'Route Not Found');

        $this->client = $client;

        $this->request = $request;

        $this->config = $request
            ->getRoute()
            ->getConfig();

        $this->actions = $request
            ->getRoute()
            ->getActions()
            ->groupBy(function ($action) {
                return $action->getSequence();
            })
            ->sortBy(function ($batch, $key) {
                return intval($key);
            });

    }

    public function get()
    {
        if (request()->getRoute()->isAggregate()) return $this->aggregateRequest();
        return $this->simpleRequest();
    }

    public function post()
    {
        if (request()->getRoute()->isAggregate()) return $this->aggregateRequest();
        return $this->simpleRequest();
    }

    public function delete()
    {
        if (request()->getRoute()->isAggregate()) return $this->aggregateRequest();

        return $this->simpleRequest();
    }

    public function put()
    {
        if (request()->getRoute()->isAggregate()) return $this->aggregateRequest();
        return $this->simpleRequest();
    }

    private function simpleRequest()
    {

        if (request()->getRoute()->isAggregate()) abort(404, 'Aggregate ' . strtoupper(request()->method()) . 's are not implemented yet');


        $this->client->setBody($this->request->all());

        if(count(request()->allFiles()) !== 0) {
            $this->client->setFiles($this->request->all());
        }

        $parametersJar = array_merge(request()->getRouteParams(), ['query_string' => request()->getQueryString()]);
        $response = $this->client->syncRequest($this->actions->first()->first(), $parametersJar)->getBody()->getContents();

        return response()->json(json_decode($response));
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    private function aggregateRequest()
    {

        // Aggregate Request
        $parametersJar = array_merge(request()->getRouteParams(), ['query_string' => request()->getQueryString()]);

        // Initial Body
        if (request()->getContent() != "") {
            $parametersJar = array_merge($parametersJar, $this->client->setAggregateOriginBody(request()->getContent()));
        }

        $output = $this->actions->reduce(function ($carry, $batch) use (&$parametersJar) {

            $parametersJar = array_merge($parametersJar, $this->importParameters($parametersJar, $batch[0]));

            $responses = $this->client->asyncRequest($batch, $parametersJar);
            $parametersJar = array_merge($parametersJar, $responses->exportParameters($batch[0]));
            return array_merge($carry, $responses->getResponses()->toArray());
        }, []);
        $output = (!empty($this->config['merge']) && $this->config['merge'] === 'DESC') ? array_reverse($output) : $output;
        return response()->json($this->rearrangeKeys($output));
    }


    /**
     * @param array $output
     * @return array
     */
    private function rearrangeKeys(array $output)
    {
        return collect(array_keys($output))->reduce(function ($carry, $alias) use ($output) {
            $key = $this->config['actions'][$alias]['output_key'] ?? $alias;
            if ($key === false) return $carry;

            $data = isset($this->config['actions'][$alias]['input_key']) ? $output[$alias][$this->config['actions'][$alias]['input_key']] : $output[$alias];


            /*
            |--------------------------------------------------------------------------
            | Merge Action True
            |--------------------------------------------------------------------------
            */
            if (isset($this->config['merge']) && isset($this->config['actions'][$alias]['merge_key']) && !isset($data['error'])) {
                [$key1, $key2] = explode('%', $this->config['actions'][$alias]['merge_key']);
                $data = $output[$alias]['data'];

                if (isset($carry['data']['data'][0]) || isset($carry['data'][0])) {

                    $res = isset($carry['data']['data']) ? $carry['data']['data'] : $carry['data'];
                    $result = collect($res)->map(function ($item) use ($key1, $key2, $data, $alias) {

                        $val = $item[$key1];

                        $filtered = Arr::where($data, function ($value) use ($key2, $val) {
                            return $value[$key2] == $val;
                        });

                        if ($filtered){
                            $item[$alias] = array_values($filtered);
                        }
                        return $item;
                    });
                } else {
                    $res = isset($carry['data']['data']) ? $carry['data']['data'] : $carry['data'];
                    if(!empty($res))
                        $result = collect($res)->merge([
                            $alias => array_values($data)
                        ]);
                }
                if (empty($result) && empty($result[$alias])){
                    return isset($carry['data']['data']) ? collect($carry['data']) : collect($carry);
                }
                return $carry = ['data' => $result];
            }

            if (!isset($this->config['merge']) && !isset($this->config['actions'][$alias]['merge_key']) && !isset($data['error'])) {
                return $carry = $output[$alias];
            }


            if (empty($key)) {
                return array_merge($carry, $data);
            }

            if (is_string($key)) {
                Arr::set($carry, $key, $data);
            }

            if (is_array($key)) {
                collect($key)->each(function ($outputKey, $property) use (&$data, &$carry, $key) {
                    if ($property == '*') {
                        $carry[$outputKey] = $data;
                        return;
                    }
                    if (isset($data[$property])) {
                        Arr::set($carry, $outputKey, $data[$property]);
                        unset($data[$property]);
                    }
                });
            }

            return $carry;
        }, []);


    }


    /**
     * @param $parametersJar
     * @param $batch
     * @return array
     */
    public function importParameters($parametersJar, $batch)
    {

        return collect($parametersJar)->reduce(function ($carry, $value, $alias) use ($batch) {

            $output = [];

            if (empty($value))
                return $carry;


            if ($explore = $batch->getImport()) {
                foreach ($explore as $key => $val) {

                    if (!is_array($value))
                        continue;


                    $result = collect($value)->keyBy($val)->keys()->toArray();


                    $result = collect($result)->filter(function ($val) {
                        return !empty($val);
                    })->toArray();

                    if (collect($result)->isEmpty())
                        continue;

                    $result = implode(",", $result);

                    $output[$alias . '%' . $val] = $result;
                }


            }

            return array_merge($carry, $output);
        }, []);

    }

}
