<?php

namespace App\Http\Middleware;

use App\Classes\Request\Request;
use App\Classes\Route\RouteRegistry;
use Closure;

class CheckRoute
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @param $id
     * @return mixed
     */
    public function handle(Request $request, Closure $next,$id)
    {

        $request->attachRoute(
            app()->make(RouteRegistry::class)->getRoute($id)
        );

        return $next($request);
    }
}
