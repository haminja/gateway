<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {

        /*
       |--------------------------------------------------------------------------
       | Validation Exception
       |--------------------------------------------------------------------------
       */
        if ($exception instanceof ValidationException) {
            return response()->json([
                'error' => [
                    'message' => json_decode($exception->getResponse()->getContent()),
                    'status'  => $exception->getResponse()->getStatusCode()
                ]
            ]);
        }

        /*
        |--------------------------------------------------------------------------
        | All Exception Handler
        |--------------------------------------------------------------------------
        */
        $code = method_exists($exception, 'getStatusCode') ? $exception->getStatusCode() : $exception->getCode();
        $message = $this->getMessageException($code, $exception);


        return response()->json([
            'error' => [
                'message' => $message,
                'status'  => $code,
                'trace'   => $exception->getTrace()
            ]
        ]);
    }


    public function getMessageException($code, $exception)
    {

        switch ($code) {
            case 404 :
            {
                $message = $exception->getMessage() ? $exception->getMessage() : 'route not found';
                break;
            }
            case 405 :
            {
                $message = $exception->getMessage() ? $exception->getMessage() : 'method not allowed http';
                break;
            }
            case 422:
            {
                $message = json_decode($exception->getResponse()->getContent());
                break;
            }
            default :
            {
                $message = $exception->getMessage();
            }
        }

        return $message;
    }
}
