<?php
return [

    'services' => [
        'auth' => [
            'hostname' => 'auth.profatest.ir'
        ],
        'financial' => [
            'hostname' => 'financial.profatest.ir'
        ],
        'blog' => [
            'hostname' => 'blog.profatest.ir'
        ],
        'market' => [
            'hostname' => 'market.profatest.ir'
        ]
    ],


    'routes' => [
        /*
        |--------------------------------------------------------------------------
        | Api Login
        |--------------------------------------------------------------------------
        */
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/login',
            'public' => 'true',
            'actions' => [
                'login' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'login',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/register',
            'public' => true,
            'actions' => [
                'register' => [
                    'service' => 'auth',
                    'method' => 'POST',
                    'path' => 'register',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'DELETE',
            'path' => '/logout',
            'actions' => [
                'logout' => [
                    'service' => 'auth',
                    'method' => 'DELETE',
                    'path' => 'logout',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        /*
        |--------------------------------------------------------------------------
        | Api User
        |--------------------------------------------------------------------------
        */
        [
            'aggregate' => true,
            'method' => 'GET',
            'path' => '/user',
            'merge' => true,
            'actions' => [
                'user' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'user',
                    'sequence' => 0,
                    'critical' => true,
                    'output_key' => 'data',
                    'export' => [
                        'user_id'
                    ]
                ],
                'credit' => [
                    'service' => 'financial',
                    'method' => 'GET',
                    'path' => 'credit',
                    'sequence' => 1,
                    'critical' => true,
                    'import' => [
                        'user_id'
                    ],
                    'query_string' => [
                        'type' => '{"type":"exec","condition":"=","value":"User"}',
                        'type_id' => '{"type":"execIn","value":"{user%user_id}"}'
                    ],
                    'merge_key' => 'user_id%type_id'
                ],
            ]
        ],
        [
            'aggregate' => true,
            'method' => 'GET',
            'path' => '/user/show',
            'merge' => true,
            'actions' => [
                'user' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'user/show',
                    'sequence' => 0,
                    'critical' => true,
                    'output_key' => 'data',
                    'export' => [
                        'user_id'
                    ]
                ],
                'credit' => [
                    'service' => 'financial',
                    'method' => 'GET',
                    'path' => 'credit',
                    'sequence' => 1,
                    'critical' => true,
                    'import' => [
                        'user_id'
                    ],
                    'query_string' => [
                        'type' => '{"type":"exec","condition":"=","value":"App\Models\User"}',
                        'type_id' => '{"type":"execIn","value":"{user%user_id}"}'
                    ],
                    'merge_key' => 'user_id%type_id'
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/user',
            'actions' => [
                'user' => [
                    'service' => 'auth',
                    'method' => 'POST',
                    'path' => 'user',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'PUT',
            'path' => '/user/{id}',
            'actions' => [
                'user' => [
                    'service' => 'auth',
                    'method' => 'POST',
                    'path' => '/user/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'DELETE',
            'path' => '/user/{id}',
            'actions' => [
                'user' => [
                    'service' => 'auth',
                    'method' => 'DELETE',
                    'path' => 'user/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        /*
        |--------------------------------------------------------------------------
        | Api Role
        |--------------------------------------------------------------------------
        */
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/role',
            'actions' => [
                'user' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'role',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/role/{id}',
            'actions' => [
                'show' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'role/{id}',
                    'sequence' => 0,
                    'critical' => true,
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/role',
            'actions' => [
                'user' => [
                    'service' => 'auth',
                    'method' => 'POST',
                    'path' => 'role',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'PUT',
            'path' => '/role/{id}',
            'actions' => [
                'user' => [
                    'service' => 'auth',
                    'method' => 'PUT',
                    'path' => '/role/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'DELETE',
            'path' => '/role/{id}',
            'actions' => [
                'user' => [
                    'service' => 'auth',
                    'method' => 'DELETE',
                    'path' => 'role/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/userRole',
            'actions' => [
                'user' => [
                    'service' => 'auth',
                    'method' => 'POST',
                    'path' => 'userRole',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'DELETE',
            'path' => '/userRole',
            'actions' => [
                'user' => [
                    'service' => 'auth',
                    'method' => 'DELETE',
                    'path' => 'userRole',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/checkRole',
            'actions' => [
                'user' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'checkRole',
                    'sequence' => 0,
                    'critical' => true
                ],

            ]
        ],

        /*
        |--------------------------------------------------------------------------
        | Api Organization
        |--------------------------------------------------------------------------
        */
        [
            'aggregate' => true,
            'method' => 'GET',
            'path' => '/org',
            'public' => true,
            'merge' => true,
            'actions' => [
                'org' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'org',
                    'sequence' => 0,
                    'critical' => true,
                    'output_key' => 'data',
                    'export'=>[
                        'organization_id'
                    ]
                ],
                'credit' => [
                    'service' => 'financial',
                    'method' => 'GET',
                    'path' => 'credit',
                    'sequence' => 1,
                    'critical' => true,
                    'import' => [
                        'organization_id'
                    ],
                    'query_string' => [
                        'type' => '{"type":"exec","condition":"=","value":"App\Model\Organization"}',
                        'type_id' => '{"type":"execIn","value":"{org%organization_id%organization_id}"}'
                    ],
                    'merge_key' => 'organization_id%type_id'
                ],
            ]
        ],
        [
            'aggregate' => true,
            'method' => 'GET',
            'path' => '/org/{id}',
            'public' => true,
            'merge' => true,
            'actions' => [
                'org' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'org/{id}',
                    'sequence' => 0,
                    'critical' => true,
                    'output_key' => 'data',
                    'export'=>[
                        'organization_id'
                    ]

                ],
                'credit' => [
                    'service' => 'financial',
                    'method' => 'GET',
                    'path' => 'credit',
                    'sequence' => 1,
                    'critical' => true,
                    'import' => [
                        'organization_id'
                    ],
                    'query_string' => [
                        'type' => '{"type":"exec","condition":"=","value":"App\Model\Organization"}',
                        'type_id' => '{"type":"exec","condition":"=","value":"{org%organization_id}"}'
                    ],
                    'merge_key' => 'organization_id%type_id'
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/org',
            'actions' => [
                'org' => [
                    'service' => 'auth',
                    'method' => 'POST',
                    'path' => 'org',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'PUT',
            'path' => '/org/{id}',
            'actions' => [
                'org' => [
                    'service' => 'auth',
                    'method' => 'POST',
                    'path' => '/org/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'DELETE',
            'path' => '/org/{id}',
            'actions' => [
                'org' => [
                    'service' => 'auth',
                    'method' => 'DELETE',
                    'path' => 'org/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => 'checkBrand',
            'actions' => [
                'check' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'checkBrand',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        /*
        |--------------------------------------------------------------------------
        | Api Category
        |--------------------------------------------------------------------------
        */
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/category',
            'public' => true,
            'actions' => [
                'category' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'category',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/category/{id}',
            'public' => true,
            'actions' => [
                'category' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'category/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/category',
            'actions' => [
                'category' => [
                    'service' => 'auth',
                    'method' => 'POST',
                    'path' => 'category',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'PUT',
            'path' => '/category/{id}',
            'actions' => [
                'category' => [
                    'service' => 'auth',
                    'method' => 'PUT',
                    'path' => '/category/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'DELETE',
            'path' => '/category/{id}',
            'actions' => [
                'category' => [
                    'service' => 'auth',
                    'method' => 'DELETE',
                    'path' => 'category/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        /*
        |--------------------------------------------------------------------------
        | Api City
        |--------------------------------------------------------------------------
        */
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/city',
            'public' => true,
            'actions' => [
                'category' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'city',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        /*
        |--------------------------------------------------------------------------
        | Api Address
        |--------------------------------------------------------------------------
        */
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/address',
            'public' => true,
            'actions' => [
                'address' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'address',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/address/{id}',
            'public' => true,
            'actions' => [
                'show' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'address/{id}',
                    'sequence' => 0,
                    'critical' => true,
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/address',
            'actions' => [
                'user' => [
                    'service' => 'auth',
                    'method' => 'POST',
                    'path' => 'address',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'PUT',
            'path' => '/address/{id}',
            'actions' => [
                'user' => [
                    'service' => 'auth',
                    'method' => 'PUT',
                    'path' => '/address/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'DELETE',
            'path' => '/address/{id}',
            'actions' => [
                'user' => [
                    'service' => 'auth',
                    'method' => 'DELETE',
                    'path' => 'address/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],


        /*
        |--------------------------------------------------------------------------
        | Api Province
        |--------------------------------------------------------------------------
        */
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/province',
            'public' => true,
            'actions' => [
                'category' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'province',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        /*
        |--------------------------------------------------------------------------
        | Api Tag
        |--------------------------------------------------------------------------
        */
        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/tag',
            'actions' => [
                'tag' => [
                    'service' => 'auth',
                    'method' => 'POST',
                    'path' => 'tag',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'DELETE',
            'path' => '/tag',
            'actions' => [
                'tag' => [
                    'service' => 'auth',
                    'method' => 'DELETE',
                    'path' => 'tag',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        /*
        |--------------------------------------------------------------------------
        | Api Plan
        |--------------------------------------------------------------------------
        */
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/plan',
            'actions' => [
                'plan' => [
                    'service' => 'financial',
                    'method' => 'GET',
                    'path' => 'plan',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/plan/{id}',
            'actions' => [
                'plan' => [
                    'service' => 'financial',
                    'method' => 'GET',
                    'path' => 'plan/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/plan',
            'actions' => [
                'plan' => [
                    'service' => 'financial',
                    'method' => 'POST',
                    'path' => 'plan',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'PUT',
            'path' => '/plan/{id}',
            'actions' => [
                'plan' => [
                    'service' => 'financial',
                    'method' => 'PUT',
                    'path' => '/plan/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'DELETE',
            'path' => '/plan/{id}',
            'actions' => [
                'plan' => [
                    'service' => 'financial',
                    'method' => 'DELETE',
                    'path' => 'plan/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        /*
        |--------------------------------------------------------------------------
        | Api Payment
        |--------------------------------------------------------------------------
        */
        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/payment',
            'actions' => [
                'plan' => [
                    'service' => 'financial',
                    'method' => 'POST',
                    'path' => 'payment',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/callback',
            'public' => true,
            'actions' => [
                'plan' => [
                    'service' => 'financial',
                    'method' => 'POST',
                    'path' => 'callback',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        /*
        |--------------------------------------------------------------------------
        | API Invoice
        |--------------------------------------------------------------------------
        */
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/invoice',
            'actions' => [
                'invoice' => [
                    'service' => 'financial',
                    'method' => 'GET',
                    'path' => 'invoice',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/invoice/{id}',
            'actions' => [
                'invoice' => [
                    'service' => 'financial',
                    'method' => 'GET',
                    'path' => 'invoice/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/invoice',
            'actions' => [
                'invoice' => [
                    'service' => 'financial',
                    'method' => 'POST',
                    'path' => 'invoice',
                    'sequence' => 1,
                    'critical' => true,
                ],
            ]
        ],
        [
            'aggregate' => true,
            'method' => 'POST',
            'path' => '/invoice/product',
            'actions' => [
                'product' => [
                    'service' => 'market',
                    'method' => 'GET',
                    'path' => 'product',
                    'sequence' => 0,
                    'critical' => true,
                    'output_key' => 'data',
                    'query_string' => [
                        'product_id' => '{"type":"execIn","value":"{origin%items%type_id}"}'
                    ],
                    'import' => [
                        'type_id'
                    ],
                    'export' => [
                        'items' => [
                            'type_id' => 'product_id',
                            'price' => 'price',
                            'type' => 'product',
                            'price_discount' => 'price_discount'
                        ]
                    ]
                ],
                'invoice' => [
                    'service' => 'financial',
                    'method' => 'POST',
                    'path' => 'invoice',
                    'sequence' => 1,
                    'critical' => true,
                    'body' => [
                        'items' => '{product%items}',
                        'product' => '{origin%items}'
                    ]
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'PUT',
            'path' => '/invoice/{id}',
            'actions' => [
                'invoice' => [
                    'service' => 'financial',
                    'method' => 'PUT',
                    'path' => '/invoice/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'DELETE',
            'path' => '/invoice/{id}',
            'actions' => [
                'invoice' => [
                    'service' => 'financial',
                    'method' => 'DELETE',
                    'path' => 'invoice/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        /*
        |--------------------------------------------------------------------------
        | API Transaction
        |--------------------------------------------------------------------------
        */
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/transaction',
            'actions' => [
                'transaction' => [
                    'service' => 'financial',
                    'method' => 'GET',
                    'path' => 'transaction',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/transaction/{id}',
            'actions' => [
                'transaction' => [
                    'service' => 'financial',
                    'method' => 'GET',
                    'path' => 'transaction/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/transaction',
            'actions' => [
                'transaction' => [
                    'service' => 'financial',
                    'method' => 'POST',
                    'path' => 'transaction',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'PUT',
            'path' => '/transaction/{id}',
            'actions' => [
                'transaction' => [
                    'service' => 'financial',
                    'method' => 'PUT',
                    'path' => '/transaction/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'DELETE',
            'path' => '/transaction/{id}',
            'actions' => [
                'transaction' => [
                    'service' => 'financial',
                    'method' => 'DELETE',
                    'path' => 'transaction/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        /*
        |--------------------------------------------------------------------------
        | API Setting Financial
        |--------------------------------------------------------------------------
        */
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/financial/setting',
            'actions' => [
                'transaction' => [
                    'service' => 'financial',
                    'method' => 'GET',
                    'path' => 'setting',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        /*
        |--------------------------------------------------------------------------
        | API Transfer Financial
        |--------------------------------------------------------------------------
        */
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/financial/transfer',
            'actions' => [
                'transfer' => [
                    'service' => 'financial',
                    'method' => 'GET',
                    'path' => 'transfer',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        /*
        |--------------------------------------------------------------------------
        | API credit Financial
        |--------------------------------------------------------------------------
        */
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/financial/credit',
            'actions' => [
                'credit' => [
                    'service' => 'financial',
                    'method' => 'GET',
                    'path' => 'credit',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        /*
        |--------------------------------------------------------------------------
        | Api Shipping
        |--------------------------------------------------------------------------
        */
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/financial/shipping',
            'public' => true ,
            'actions' => [
                'user' => [
                    'service' => 'financial',
                    'method' => 'GET',
                    'path' => 'shipping',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/financial/shipping/{id}',
            'public' => true ,
            'actions' => [
                'show' => [
                    'service' => 'financial',
                    'method' => 'GET',
                    'path' => 'shipping/{id}',
                    'sequence' => 0,
                    'critical' => true,
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/financial/shipping',
            'actions' => [
                'user' => [
                    'service' => 'financial',
                    'method' => 'POST',
                    'path' => 'shipping',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'PUT',
            'path' => '/financial/shipping/{id}',
            'actions' => [
                'user' => [
                    'service' => 'financial',
                    'method' => 'PUT',
                    'path' => '/shipping/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'DELETE',
            'path' => '/financial/shipping/{id}',
            'actions' => [
                'user' => [
                    'service' => 'financial',
                    'method' => 'DELETE',
                    'path' => 'shipping/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],


        /*
        |--------------------------------------------------------------------------
        | API Post
        |--------------------------------------------------------------------------
        */

        [
            'aggregate' => true,
            'method' => 'GET',
            'path' => '/post',
            'public' => true,
            'merge' => true,
            'actions' => [
                'post' => [
                    'service' => 'blog',
                    'method' => 'GET',
                    'path' => 'post',
                    'sequence' => 0,
                    'critical' => true,
                    'output_key' => 'data',
                    'export' => [
                        'organization_id'
                    ]
                ],
                'org' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'org',
                    'sequence' => 1,
                    'critical' => true,
                    'query_string' => [
                        'organization_id' => '{"type":"execIn","value":"{post%organization_id}"}'
                    ],
                    'merge_key' => 'organization_id%organization_id'
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/post/show',
            'public' => true,
            'actions' => [
                'post' => [
                    'service' => 'blog',
                    'method' => 'GET',
                    'path' => 'post/show',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/post',
            'actions' => [
                'post' => [
                    'service' => 'blog',
                    'method' => 'POST',
                    'path' => 'post',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'PUT',
            'path' => '/post/{id}',
            'actions' => [
                'post' => [
                    'service' => 'blog',
                    'method' => 'POST',
                    'path' => 'post/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'DELETE',
            'path' => '/post/{id}',
            'actions' => [
                'post' => [
                    'service' => 'blog',
                    'method' => 'DELETE',
                    'path' => 'post/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        [
            'aggregate' => true,
            'method' => 'GET',
            'path' => '/post/time',
            'public' => true,
            'merge' => true,
            'actions' => [
                'post' => [
                    'service' => 'blog',
                    'method' => 'GET',
                    'path' => 'post/time',
                    'sequence' => 0,
                    'critical' => true,
                    'output_key' => 'data',
                    'export' => [
                        'organization_id'
                    ]
                ],
                'org' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'org',
                    'sequence' => 1,
                    'critical' => true,
                    'import' => [
                        'organization_id'
                    ],
                    'query_string' => [
                        'organization_id' => '{"type":"execIn","value":"{post%organization_id%organization_id}"}'
                    ],
                    'merge_key' => 'organization_id%organization_id'
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'PUT',
            'path' => '/post/time/{id}',
            'actions' => [
                'post' => [
                    'service' => 'blog',
                    'method' => 'PUT',
                    'path' => 'post/time/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'DELETE',
            'path' => '/post/time/{id}',
            'actions' => [
                'post' => [
                    'service' => 'blog',
                    'method' => 'DELETE',
                    'path' => 'post/time/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],


        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/post/like',
            'actions' => [
                'post' => [
                    'service' => 'blog',
                    'method' => 'POST',
                    'path' => 'post/like',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/post/unlike',
            'actions' => [
                'post' => [
                    'service' => 'blog',
                    'method' => 'POST',
                    'path' => 'post/unlike',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/post/comment',
            'actions' => [
                'post' => [
                    'service' => 'blog',
                    'method' => 'POST',
                    'path' => 'post/comment',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'DELETE',
            'path' => '/post/comment/{id}',
            'actions' => [
                'post' => [
                    'service' => 'blog',
                    'method' => 'DELETE',
                    'path' => 'post/comment/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],


        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/post/type/all',
            'actions' => [
                'post' => [
                    'service' => 'blog',
                    'method' => 'GET',
                    'path' => '/post/type/all',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'PUT',
            'path' => 'post/change/status',
            'actions' => [
                'post' => [
                    'service' => 'blog',
                    'method' => 'PUT',
                    'path' => 'post/change/status',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],


        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => 'post/report',
            'actions' => [
                'post' => [
                    'service' => 'blog',
                    'method' => 'GET',
                    'path' => 'post/report',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => 'post/report/create',
            'actions' => [
                'post' => [
                    'service' => 'blog',
                    'method' => 'POST',
                    'path' => 'post/report/create',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],








        /*
       |--------------------------------------------------------------------------
       | API Market
       |--------------------------------------------------------------------------
       */

        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/market/category',
            'actions' => [
                'post' => [
                    'service' => 'market',
                    'method' => 'GET',
                    'path' => 'category',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/market/category/{id}',
            'actions' => [
                'post' => [
                    'service' => 'market',
                    'method' => 'GET',
                    'path' => 'category/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/market/unit',
            'actions' => [
                'post' => [
                    'service' => 'market',
                    'method' => 'GET',
                    'path' => 'unit',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'GET',
            'path' => '/market/unit/{id}',
            'actions' => [
                'post' => [
                    'service' => 'market',
                    'method' => 'GET',
                    'path' => 'unit/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],


        /*
        |--------------------------------------------------------------------------
        | Api Product
        |--------------------------------------------------------------------------
        |
        */


        [
            'aggregate' => true,
            'method' => 'GET',
            'path' => 'product',
            'public' => true,
            'merge' => true,
            'actions' => [
                'product' => [
                    'service' => 'market',
                    'method' => 'GET',
                    'path' => 'product',
                    'sequence' => 0,
                    'critical' => true,
                    'output_key' => 'data',
                    'export' => [
                        'organization_id'
                    ]
                ],
                'org' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'org',
                    'sequence' => 1,
                    'critical' => true,
                    'import' => [
                        'organization_id'
                    ],
                    'query_string' => [
                        'organization_id' => '{"type":"execIn","value":"{product%organization_id%organization_id}"}'
                    ],
                    'merge_key' => 'organization_id%organization_id'
                ],
            ]
        ],

        [
            'aggregate' => true,
            'method' => 'GET',
            'path' => 'market/product/org',
            'public' => true,
            'merge' => 'DESC',
            'actions' => [
                'org' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'org',
                    'sequence' => 0,
                    'critical' => true,
                    'output_key' => 'data',
                    'export' => [
                        'organization_id'
                    ],
                    'merge_key' => 'organization_id%organization_id'
                ],
                'product' => [
                    'service' => 'market',
                    'method' => 'GET',
                    'path' => 'product',
                    'sequence' => 1,
                    'critical' => true,
                    'output_key' => 'data',
                    'import'=>[
                        'organization_id'
                    ],
                    'query_string' => [
                        'organization_id' => '{"type":"execIn","value":"{org%organization_id%organization_id}"}'
                    ]
                ]
            ]
        ],

        [
            'aggregate' => true,
            'method' => 'GET',
            'path' => 'product/{id}',
            'public' => true,
            'merge' => true,
            'actions' => [
                'product' => [
                    'service' => 'market',
                    'method' => 'GET',
                    'path' => 'product/{id}',
                    'sequence' => 0,
                    'critical' => true,
                    'output_key' => 'data',
                    'export' => [
                        'organization_id'
                    ]
                ],
                'org' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'org',
                    'sequence' => 1,
                    'critical' => true,
                    'import' => [
                        'organization_id'
                    ],
                    'query_string' => [
                        'organization_id' => '{"type":"execIn","value":"{product%organization_id}"}'
                    ],
                    'merge_key' => 'organization_id%organization_id'
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => 'product',
            'actions' => [
                'post' => [
                    'service' => 'market',
                    'method' => 'POST',
                    'path' => 'product',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'DELETE',
            'path' => 'product/{id}',
            'actions' => [
                'post' => [
                    'service' => 'market',
                    'method' => 'DELETE',
                    'path' => 'product/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'PUT',
            'path' => 'product/{id}',
            'actions' => [
                'post' => [
                    'service' => 'market',
                    'method' => 'POST',
                    'path' => 'product/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],


        /*
        |--------------------------------------------------------------------------
        | Api Order
        |--------------------------------------------------------------------------
        |
        */

        [
            'aggregate' => true,
            'method' => 'GET',
            'path' => 'market/order',
            'merge' => true,
            'actions' => [
                'order' => [
                    'service' => 'market',
                    'method' => 'GET',
                    'path' => 'order',
                    'sequence' => 0,
                    'critical' => true,
                    'output_key' => 'data',
                    'export'=> [
                        'shipping_id',
                        'address_id'
                    ]
                ],
                'address' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'address',
                    'sequence' => 2,
                    'critical' => true,
                    'import' => [
                        'address_id'
                    ],
                    'query_string' => [
                        'address_id' => '{"type":"execIn","value":"{order%address_id%address_id}"}'
                    ],
                    'merge_key' => 'address_id%address_id'
                ],
                'shipping' => [
                    'service' => 'financial',
                    'method' => 'GET',
                    'path' => 'shipping',
                    'sequence' => 3,
                    'critical' => true,
                    'import' => [
                        'shipping_id'
                    ],
                    'query_string' => [
                        'shipping_id' => '{"type":"execIn","value":"{order%shipping_id%shipping_id}"}'
                    ],
                    'merge_key' => 'shipping_id%shipping_id'
                ],
            ]
        ],

        [
            'aggregate' => true,
            'method' => 'GET',
            'path' => 'market/order/{id}',
            'merge'=> true,
            'actions' => [
                'order' => [
                    'service' => 'market',
                    'method' => 'GET',
                    'path' => 'order/{id}',
                    'sequence' => 0,
                    'critical' => true,
                    'output_key' => 'data',
                    'export'=> [
                        'shipping_id',
                        'address_id'
                    ]
                ],
                'address' => [
                    'service' => 'auth',
                    'method' => 'GET',
                    'path' => 'address',
                    'sequence' => 2,
                    'critical' => true,
                    'import' => [
                        'address_id'
                    ],
                    'query_string' => [
                        'address_id' => '{"type":"execIn","value":"{order%address_id}"}'
                    ],
                    'merge_key' => 'address_id%address_id'
                ],
                'shipping' => [
                    'service' => 'financial',
                    'method' => 'GET',
                    'path' => 'shipping',
                    'sequence' => 3,
                    'critical' => true,
                    'import' => [
                        'shipping_id'
                    ],
                    'query_string' => [
                        'shipping_id' => '{"type":"execIn","value":"{order%shipping_id}"}'
                    ],
                    'merge_key' => 'shipping_id%shipping_id'
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/market/order',
            'actions' => [
                'post' => [
                    'service' => 'market',
                    'method' => 'POST',
                    'path' => 'order',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],
        [
            'aggregate' => false,
            'method' => 'POST',
            'path' => '/market/order/transitions',
            'actions' => [
                'post' => [
                    'service' => 'market',
                    'method' => 'POST',
                    'path' => 'order/transitions',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'DELETE',
            'path' => '/market/order/{id}',
            'actions' => [
                'post' => [
                    'service' => 'market',
                    'method' => 'DELETE',
                    'path' => 'order/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],

        [
            'aggregate' => false,
            'method' => 'PUT',
            'path' => '/market/order/{id}',
            'actions' => [
                'post' => [
                    'service' => 'market',
                    'method' => 'PUT',
                    'path' => 'order/{id}',
                    'sequence' => 0,
                    'critical' => true
                ],
            ]
        ],


    ],

];
